import { menuArray } from "./data.js"
const menu = document.getElementById("menu")

function render() {
  let menuHtml = ``
  menuArray.forEach(function (item) {
    menuHtml += `
        <div class="menu-container" id="menu-container">
            <div class="img-description-container">
                <div class="image-container">
                    <p class="item-img">${item.emoji}</p>
                </div>
                <div class="menu-items-container">
                    <ul class="menu-items">
                        <li class="menu-item">
                        <h2 class="item-title">${item.name}</h2>
                        </li>
                        <li class="menu-item">
                        <p class="item-description">${item.ingredients}</p>
                        </li>
                        <li class="menu-item">
                        $<span class="item-price">${item.price}</span>
                        </li>
                    </ul>
                </div>
            </div>
                <div class="add-btn">
                    <i class="fa-regular fa-square-plus"></i>
                </div>
        </div>`
  })

  menu.innerHTML = menuHtml
}

render()
